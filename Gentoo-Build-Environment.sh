# If you use a grsec kernel on the host:

echo "0" > /proc/sys/kernel/grsecurity/chroot_deny_chmod
echo "0" > /proc/sys/kernel/grsecurity/chroot_caps

mkdir gentoodev
cd gentoodev
# verify fingerprints!
gpg --keyserver subkeys.pgp.net --recv-keys 96D8BF6D 2D182910 17072058

# fetch stage3 and portage snapshot
LATESTDL=`wget -q http://distfiles.gentoo.org/releases/x86/autobuilds/latest-stage3-i686.txt -O - | tail -1`
wget http://distfiles.gentoo.org/releases/x86/autobuilds/$LATESTDL{,.DIGESTS.asc,.DIGESTS}
wget http://distfiles.gentoo.org/releases/snapshots/current/portage-latest.tar.bz2{,.gpgsig,.md5sum}

gpg --verify stage3-*.tar.bz2.DIGESTS.asc
sha1sum -c stage3*.tar.bz2.DIGESTS

gpg --verify portage-latest.tar.bz2.gpgsig portage-latest.tar.bz2
md5sum -c portage-latest.tar.bz2.md5sum

tar xvjpf stage3-*.tar.bz2
tar xvjf portage-latest.tar.bz2 -C usr

# edit makeopts according to your CPU count (cores+1); add a nearby mirror (when using a gentoo install disk use #mirrorselect -i -o >> etc/make.conf
echo '
MAKEOPTS="-j3"
USE="unicode minimal crypt ssl gpm ncurses fbcon -X -pam -cracklib -git -extensions     -glib -opengl \
-icu -readline -xcb -fts3 -png -policykit -gtk -crashreporter -startup-notification \
-dbus -introspection -libnotify -webm -gnome -qt4 -qt3support -kde -bluetooth -nls     -ipv6 \
-cups -zeroconf -encode -recode -acl -iconv -tcpd -berkdb -pcre -net -cramfs -sqlite -gdbm"
FEATURES="webrsync-gpg nodoc noinfo noman"
PORTAGE_GPG_DIR="/etc/portage/gpg"
SYNC=""
#GENTOO_MIRRORS=""' >> etc/make.conf

cp -L /etc/resolv.conf etc/
mount -t proc none proc/
chroot . /bin/bash
env-update
source /etc/profile
export PS1="(build-chroot) $PS1"

# set root passwd (just used in the development environment, probably not necessary?)
passwd

eselect profile list
# select hardened/linux/x86
eselect profile set 8

echo "en_US ISO-8859-1" >> /etc/locale.gen
locale-gen
cp /usr/share/zoneinfo/UTC /etc/localtime

# get a hardened toolchain
emerge --oneshot binutils gcc virtual/libc
source /etc/profile
export PS1="(build-chroot) $PS1"

# Configure emerge-webrsync. Verify fingerprints!
emerge gnupg
mkdir -p /etc/portage/gpg
chmod 0700 /etc/portage/gpg
gpg --homedir /etc/portage/gpg --keyserver subkeys.pgp.net --recv-keys 0x239C75C4 0x96D8BF6D
gpg --homedir /etc/portage/gpg --edit-key AE5454F967B56AB09AE160640838C26E239C75C4 trust
gpg --homedir /etc/portage/gpg --edit-key DCD05B71EAB94199527F44ACDB6B8C1F96D8BF6D trust

# update portage with
emerge-webrsync
