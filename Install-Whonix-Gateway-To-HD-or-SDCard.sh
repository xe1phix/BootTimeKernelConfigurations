# Install-Whonix-Gateway-To-HD-or-SDCard.sh

Using Grub:

fdisk /dev/sdb
mkfs.ext4 /dev/sdb1
mkdir /mnt/target
mount /dev/sdb1 /mnt/target

# edit according to partition and disc layout! sda/sdb/hd are just examples!
# The /dev/sd* name will probably be different on your build system and on your target     system.
# in this example the host got two hard disks, you install to the second one, "sdb"
# this drive is attached to the gateway system and if it's the only drive it becomes "sda"
# same goes for grub: hd1 == sdb, hd0 == sda

echo "
title TorGateway
root (hd0,0)
kernel /boot/kernel root=/dev/sda1" >> /boot/grub/grub.conf

grep -v rootfs /proc/mounts > /etc/mtab

echo "/dev/sda1               /               ext4            noatime         0 1" > /etc/fstab

## outside the chroot (open a new terminal on the host)
cd gentoodev
rsync -a dev/ torgateway/dev/
rm -r torgateway/var/db/* torgateway/tmp/*
rsync -a torgateway/ /mnt/target/

## then, inside the target-chroot

grub
# then manually configure:
# grub> root (hd1,0)
# grub> setup (hd1)

# alternatively, may not work properly
#grub-install --no-floppy /dev/sdb --recheck

exit #target-chroot
exit #build-chroot
